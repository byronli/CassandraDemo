/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.nio.ByteBuffer;
import java.util.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.cassandra.thrift.*;
import org.apache.cassandra.utils.ByteBufferUtil;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CfAppSetup
{
	private static final Logger logger = LoggerFactory.getLogger(CfAppSetup.class);

	private static String filePath = "./data/inputs_5";   //保存文件的基目录，这个地方可能需要修改

	public static void main(String[] args) throws Exception
	{
		Cassandra.Iface client = createConnection();
		setupKeyspace(client);
		client.set_keyspace(CfApp.KEYSPACE);
		setupTable(client);
		insertData(client);
		System.exit(0);
	}

	private static void setupKeyspace(Cassandra.Iface client)
			throws TException
	{
		try
		{
			client.describe_keyspace(CfApp.KEYSPACE);
		}
		catch(NotFoundException e)
		{
			logger.info("set up keyspace " + CfApp.KEYSPACE);

			String query = "CREATE KEYSPACE " + CfApp.KEYSPACE +
					" WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 1}";

			client.execute_cql3_query(ByteBufferUtil.bytes(query), Compression.NONE, ConsistencyLevel.ONE);

			String verifyQuery = "select count(*) from system.peers";
			CqlResult result = client.execute_cql3_query(ByteBufferUtil.bytes(verifyQuery), Compression.NONE, ConsistencyLevel.ONE);

			long magnitude = ByteBufferUtil.toLong(result.rows.get(0).columns.get(0).value);
			try
			{
				Thread.sleep(1000 * magnitude);
			}
			catch (InterruptedException ie)
			{
				throw new RuntimeException(ie);
			}
		}
	}
	
	private static void setupTable(Cassandra.Iface client) throws TException
	{
		/*解析 COLUMN_FAMILY，创建 column family*/
		String[] columnFamilies = CfApp.COLUMN_FAMILY.split(":");

		for (String columnFamily : columnFamilies) {
			logger.info("create  table " + columnFamily);
			String query = "CREATE TABLE " + CfApp.KEYSPACE + "." + columnFamily +
					" ( id uuid," + columnFamily.toLowerCase() + " text, " + "   PRIMARY KEY (id) ) ";
			try {
				logger.info("set up table " + columnFamily);
				client.execute_cql3_query(ByteBufferUtil.bytes(query), Compression.NONE, ConsistencyLevel.ONE);
			} catch (InvalidRequestException e) {
				logger.error("failed to create table " + CfApp.KEYSPACE + "." + columnFamily, e);
			}
		}
	}

	private static Cassandra.Iface createConnection() throws TTransportException
	{
		if (System.getProperty("cassandra.host") == null || System.getProperty("cassandra.port") == null)
		{
			logger.warn("cassandra.host or cassandra.port is not defined, using default");
		}
		return createConnection(System.getProperty("cassandra.host", "localhost"),
				Integer.valueOf(System.getProperty("cassandra.port", "9160")));
	}

	private static Cassandra.Client createConnection(String host, Integer port) throws TTransportException
	{
		TSocket socket = new TSocket(host, port);
		TTransport trans = new TFramedTransport(socket);
		trans.open();
		TProtocol protocol = new TBinaryProtocol(trans);
		return new Cassandra.Client(protocol);
	}

	private static void insertData(Cassandra.Iface client) throws TException
	{
		String[] columnFamilies = CfApp.COLUMN_FAMILY.split(":");
		File file = new File(filePath);
        final int len = columnFamilies.length;
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String line;
			// 一次读入一行，直到读入null为文件结束
			while ((line = reader.readLine()) != null) {
				//将读入的一行数据加载到cassandra中
                UUID uuid = UUID.randomUUID();  // row key
                String[] items = line.split(" ");
                
				for (int i = 0; i < len; i++) {
				    String columnFamily = columnFamilies[i];
				    
				    // Prepare data to insert.
                    List<ByteBuffer> values = new ArrayList<>();
                    values.add(ByteBufferUtil.bytes(uuid));
                    
                    values.add(ByteBufferUtil.bytes(items[i]));

                    // Exec insertion.
                    logger.info("insert into table " + columnFamily);
                    String query = "INSERT INTO " + columnFamily + "(id, " + columnFamily.toLowerCase() + ") values (?, ?) ";
                    CqlPreparedResult result = client.prepare_cql3_query(ByteBufferUtil.bytes(query), Compression.NONE);
                    client.execute_prepared_cql3_query(result.itemId, values, ConsistencyLevel.ONE);
                }
			}
			reader.close();
		} catch (IOException e) {
		    System.err.println(e.toString());
		    e.printStackTrace();
		}
	}
}