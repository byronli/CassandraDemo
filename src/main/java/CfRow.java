import com.datastax.driver.core.GettableData;
import com.datastax.driver.core.Row;

import java.util.Map;
import java.util.UUID;

/**
 * Used by mapper
 */
public interface CfRow {
    boolean isCfNull(String cf);

    boolean isColNull(String cf, String col);
    boolean isColNull(String cf, int i);

    int getInt(String cf, String col);
    int getInt(String cf, int i);

    String getString(String cf, String col);
    String getString(String cf, int i);

    UUID getUUID(String cf, String col);
    UUID getUUID(String cf, int i);
}
