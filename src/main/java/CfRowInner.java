import com.datastax.driver.core.*;
import com.google.common.reflect.TypeToken;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * This class is used by RecordReader to put into data.
 * The mapper should use the CfRow to get data by public interface.
 */
public class CfRowInner implements CfRow {
    private Map<String, Row> rowByCF;  // row divided by column families.

    public CfRowInner() {
        rowByCF = new HashMap<>();
    }

    public void add(String columnFamily, Row row) {
        rowByCF.put(columnFamily, row);
    }

    @Override
    public boolean isCfNull(String cf) {
        return !rowByCF.containsKey(cf);
    }

    @Override
    public boolean isColNull(String cf, String col) {
        return isCfNull(cf) || rowByCF.get(cf).isNull(col);
    }

    @Override
    public boolean isColNull(String cf, int col) {
        return isCfNull(cf) || rowByCF.get(cf).isNull(col);
    }

    @Override
    public int getInt(String cf, String col) {
        return rowByCF.get(cf).getInt(col);
    }

    @Override
    public int getInt(String cf, int col) {
        return rowByCF.get(cf).getInt(col);
    }

    @Override
    public String getString(String cf, String col) {
        return rowByCF.get(cf).getString(col);
    }

    @Override
    public String getString(String cf, int col) {
        return rowByCF.get(cf).getString(col);
    }

    @Override
    public UUID getUUID(String cf, String col) {
        return rowByCF.get(cf).getUUID(col);
    }

    @Override
    public UUID getUUID(String cf, int col) {
        return rowByCF.get(cf).getUUID(col);
    }
}